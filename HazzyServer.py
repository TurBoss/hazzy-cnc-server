import logging

from server import HazzyServer

FORMAT = "[%(name)s][%(levelname)s]  %(message)s (%(filename)s:%(lineno)d)"

logging.basicConfig(filename='hazzy-server.log', level=logging.DEBUG, format=FORMAT)


def main():
    server = HazzyServer()
    server.run()


if __name__ == "__main__":
    main()
